# LEGUEN_Benjamin_CUBE

**CUBE**




**1. But du jeu**

CUBE est un jeu de plateforme en 2D pour PC, le but du jeu est d'obtenir le meilleur score en gravissant le plus de plateformes possibles.



**2. Commandes du jeu**

Pour déplacer le cube utilisez les flèches gauche/droite du clavier.



**3. Expérience du joueur**

Il n'y a pas de tutoriel pour ce jeu, je voulais que le joueur découvre par lui-même comment éviter de perdre et quels sont les effets des différent séléments du jeu.
Le joueur peut faire le choix des couleurs de l'affichage.




**4. Graphisme & design**

Concernant la partie graphique je suis resté sur quelque chose de relativement simple.



**5. Choix de conception**

Au cours du développement du jeu j'ai effectué de nombreux choix d'implémentation et également des retours en arrière.

J'avais développé une première version du jeu mais à la suite d’une importante modification du fonctionnement du jeu, changement au niveau de la mouvance du joueur, ce n'était plus le joueur qui monte mais les objets qui descendaient. Cependant cette version ne m'a pas convaincu j'ai donc voulu faire un retour en arrière mais n'ayant pas versionné mon projet je n'ai pas réussi à revenir au point de départ j'ai donc dû repartir de zéro.



**6. Architectures utilisées**

J'ai favorisé l'utilisation de managers afin de répartir la gestion des différentes parties du jeu, ces managers sont des singletons.
 - Soundmanager 
 - GameManager
 - EnemyManager
 - Etc

J'avais dans un premier temps rendu tous les éléments indépendants et je leur avais confié des directives afin qu'ils puissent se gérer seuls cependant j'ai rencontré des difficultés de communication. J'ai donc fait le choix d'utiliser un GameManager afin de gérer et de communiquer avec les différents éléments du jeu. Cependant l'utilisation d'un seul manager pour gérer tout les éléments rendait le code illisible et incompréhensible. J'avais créé d'une certaine manière l’anti-pattern GodClasses qui consiste à tout gérer sans réel concordance dans une seule classe. J'ai donc par la suite divisé le travail en plusieurs managers. Ainsi le code est beaucoup plus lisible (une cinquantaine de ligne de codes maximum par classes).

Concernant les prefabs du jeu, j'ai utilisé deux méthodes différentes concernant leur génération. 

- Pour les plateformes il y a 3 types mais un seul préfab, je génère les plateformes du jeu avec des probabilités, en fonction de cette probabilité une fonction Create affilie différentes propriétés à la plateforme.
Cette méthode permet de limiter le nombre de prefab et de réutiliser du code.
- Pour ce qui concerne les bonus étant donné qu'ils avaient un comportement similaire j'ai utilisé l'heritage. Cela m'a permis de réutiliser du code existant.

Je n'ai pas effectué de bench concernant ces deux méthodes mais je pense que l'héritage est peut-être plus efficace lorsque les objets sont simples.

Pour la communication entre les éléments du jeu j'ai utilisé le principe de l'observateur. Les éléments notifient les managers de leurs changements d'état.

Pour sauvegarder dans le jeu les données comme le thème choisi ou le score du jour j'ai utilisé une classe statique PlayerStats.
Pour sauvegarder le meilleur score du joueur et pour qu'il ne soit pas détruit après la fermeture du jeu, j'ai utilisé la fonctionnalité PlayerPref.
Pour ce qui est des animations de transition j'ai regardé un tutoriel sur youtube, l'animation est assez basique mais plutôt satisfaisante.



**7. Améliorations possibles**

En termes d'amélioration du jeu :
Ce qui pourrait être fait :
- Augmenter le nombre de type de plateforme
- Augmenter le nombre de type d'ennemis
- Mettre en place une IA pour les ennemis  
- Un meilleur design avec des animations
- Une meilleure musique
- La personnalisation du cube
- Ajouter des niveaux
- Ajouter des boss
- Ajouter un système de tir
- Ajouter d'autres bonus



**8. Sources**

Aide & informations
https://answers.unity.com
https://docs.unity3d.com/
https://en.wikipedia.org/wiki
https://stackoverflow.com/

Image bouton quitter
https://favpng.com/png_view/quit-button-png

Musiques de début et de fin de jeu
https://patrickdearteaga.com/arcade-music/

Pistes audios
https://mixkit.co/free-sound-effects/

Play sound upon Collision in Unity
https://www.youtube.com/watch?v=yE0JdtVTnVk

How to make AWESOME Scene Transitions in Unity!
https://www.youtube.com/watch?v=CE9VOZivb3I

Font pixel
https://www.dafont.com/fr/bitmap.php

